#! /usr/bin/env python2.7
from difflib import SequenceMatcher
from django.contrib import admin
from mesajatma.organisation.models import Organisation
from forms import OrganisationAdminForm
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from mesajatma.spam.models import SmsSpam


class OrganisationAdmin(admin.ModelAdmin):
    form = OrganisationAdminForm
    raw_id_fields = ('created_by',)
    search_fields = ('name', 'slug', 'comment')
    list_filter = ('is_active', )

    def save_model(self, request, org, form, change):
        request.moved_to = False
        dest_org = form.cleaned_data['move_to']
        if not dest_org or dest_org == org or not org.id:
            return super(OrganisationAdmin, self).save_model(request, org, form, change)
        # move org prefs
        for p in org.prefs.all():
            if not dest_org.prefs.filter(user=p.user, organisation=p.org).count():
                dest_org.prefs.add(p)
        # move spams
        dest_spams = SmsSpam.objects.filter(organisation=dest_org)
        for spam in SmsSpam.objects.filter(organisation=org):
            # check already exist in dest spams
            spam_exist = False
            for dspam in dest_spams:
                ratio = SequenceMatcher(None, spam.text, dspam.text).ratio()
                if ratio > 0.9:
                    # found:
                    spam.delete()
                    spam_exist = True
                    break
            # any similar spam not found, move spams
            if not spam_exist:
                # change the slug if exist
                dest_slugs = [s.slug for s in dest_spams]
                if spam.slug in dest_slugs:
                    spam.slug += '-2'
                spam.organisation = dest_org
                spam.save()
        # move spam reports
        for r in org.reports.all():
            dest_org.reports.add(r)
        # add old slug
        dest_org.old_slug = org.slug
        dest_org.save()
        org.delete()
        request.moved_to = dest_org.pk

    def response_change(self, request, obj, post_url_continue=None):
        if request.moved_to and '_continue' in request.POST:
            return HttpResponseRedirect(
                reverse('admin:organisation_organisation_change',
                        args=[request.moved_to])
            )
        return super(OrganisationAdmin, self).response_change(request, obj)

admin.site.register(Organisation, OrganisationAdmin)
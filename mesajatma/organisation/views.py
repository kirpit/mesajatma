#! /usr/bin/env python2.7
from django.core.urlresolvers import reverse
from django.db.models import Count
from django.http import HttpResponsePermanentRedirect
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView
from mesajatma.organisation.models import Organisation
from mesajatma.spam.models import SmsSpam, SpamTag


class OrganisationView(TemplateView):
    template_name = 'organisation.html'

    def get(self, request, slug, *args, **kwargs):
        try:
            org = Organisation.objects.annotate(
                report_count=Count('reports'),
            ).get(slug=slug, is_active=True)
        except Organisation.DoesNotExist:
            org = get_object_or_404(Organisation, old_slug=slug, is_active=True)
            return HttpResponsePermanentRedirect(reverse(
                'organisation', args=[org.slug],
            ))
        smss = SmsSpam.objects.filter(
            organisation=org,
            is_active=True,
        ).values('pk', 'slug', 'organisation__slug', 'text').annotate(
            report_count=Count('reports'),
        )
        # fetch sms ids
        sms_ids = [sms['pk'] for sms in smss]
        sms_ids.sort()
        # fetch sms tags at once
        spam_tags = SpamTag.objects.filter(
            spam_spamtagrecord_items__content_object__in=sms_ids,
        ).values(
            'counter', 'name', 'spam_spamtagrecord_items__content_object',
        )
        # beatify the tags
        for tag in spam_tags:
            tag['sms_id'] = tag['spam_spamtagrecord_items__content_object']
            del tag['spam_spamtagrecord_items__content_object']
        context = {
            'org': org,
            'smss': smss,
            'sms_tags': spam_tags,
        }
        return self.render_to_response(context)
#! /usr/bin/env python2.7
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models


class Organisation(models.Model):
    name = models.CharField(max_length=50, db_index=True)
    old_slug = models.SlugField(db_index=True, blank=True)
    slug = models.SlugField(unique=True, db_index=True)
    is_active = models.BooleanField(default=True, db_index=True)
    is_customer = models.BooleanField(default=False)
    website = models.URLField(blank=True)
    logo = models.ImageField(upload_to='logo/', blank=True, null=True)
    twitter_account = models.CharField(max_length=50, blank=True, default='')
    comment = models.TextField(blank=True)
    created_by = models.ForeignKey(User, blank=True, null=True,
                                   on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name.encode('utf-8')

    def get_absolute_url(self):
        return reverse('organisation', args=[self.slug])


class OrganisationPreferences(models.Model):
    organisation = models.ForeignKey(Organisation, related_name='prefs')
    user = models.ForeignKey(User)
    is_customer = models.BooleanField(default=False)
    share_contacts = models.BooleanField(default=True)

    class Meta:
        index_together = (
            ('organisation', 'user'),
        )
        unique_together = (
            ('organisation', 'user'),
        )

    def __str__(self):
        return '%s %s' % (self.organisation, self.user)

    @property
    def org(self):
        return self.organisation
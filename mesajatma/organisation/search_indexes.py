#! /usr/bin/env python2.7
from haystack import indexes
from unidecode import unidecode
from mesajatma.organisation.models import Organisation


class OrganisationIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document=True)
    org_name = indexes.CharField(model_attr='name', indexed=False)
    org_slug = indexes.CharField(model_attr='slug', indexed=False)
    is_active = indexes.BooleanField(model_attr='is_active', indexed=False)
    logo = indexes.CharField(model_attr='logo', indexed=False)

    def get_model(self):
        return Organisation

    def prepare_text(self, obj):
        return unidecode(obj.name).strip().lower()
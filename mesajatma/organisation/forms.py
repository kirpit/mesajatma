#! /usr/bin/env python2.7
from django import forms
from django.contrib.admin.widgets import ForeignKeyRawIdWidget
from django.contrib.admin.sites import site as admin_site
from models import Organisation, OrganisationPreferences


class OrganisationAdminForm(forms.ModelForm):
    # custom field not backed by database
    move_to = forms.ModelChoiceField(
        queryset=Organisation.objects.all(), required=False,
        widget=ForeignKeyRawIdWidget(
            OrganisationPreferences._meta.get_field('organisation').rel,
            admin_site,
        ),
    )

    class Meta:
        model = Organisation
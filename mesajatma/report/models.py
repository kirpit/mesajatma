#! /usr/bin/env python2.7
from django.contrib.auth.models import User
from django.db import models
from mesajatma.organisation.models import Organisation
from mesajatma.spam.models import Spam


class SpamReport(models.Model):
    organisation = models.ForeignKey(Organisation, related_name='reports')
    spam = models.ForeignKey(Spam, related_name='reports')
    user = models.ForeignKey(User, related_name='reports')
    org_informed = models.BooleanField(default=False)
    is_resolved = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        ordering = ['-created_at']
        index_together = (
            ('spam', 'user'),
        )
        unique_together = (
            ('spam', 'user'),
        )

    def __str__(self):
        return '%s %s' % (self.spam, self.user)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.organisation = self.spam.organisation
        return super(SpamReport, self).save(
            force_insert, force_update, using, update_fields,
        )

    @property
    def org(self):
        return self.organisation

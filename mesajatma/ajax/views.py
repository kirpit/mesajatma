#! /usr/bin/env python2.7
from difflib import SequenceMatcher
import json
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseBadRequest, Http404
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext as _
from django.views.generic.detail import BaseDetailView
from haystack.query import SearchQuerySet
from jsonpickle.pickler import Pickler
from taggit.models import TagBase
from unidecode import unidecode
from mesajatma.lib.utils import prepare_slug
from mesajatma.organisation.models import Organisation
from mesajatma.report.models import SpamReport
from mesajatma.spam.models import SpamTagRecord, SpamTag, SmsSpam


class JSONResponseMixin(object):
    def render_to_response(self, context, **response_kwargs):
        """Returns a JSON response containing 'context' as payload"""
        content = self.convert_context_to_json(context)
        # jsonp support
        if 'request' in response_kwargs:
            request = response_kwargs['request']
            del response_kwargs['request']
            callback = request.GET.get('callback')
            if callback:
                content = '%s(%s);' % (callback, content)
        return self.get_json_response(content, **response_kwargs)

    def get_json_response(self, content, **response_kwargs):
        """Construct an `HttpResponse` object"""
        return HttpResponse(
            content,
            content_type='application/json',
            **response_kwargs
        )

    def convert_context_to_json(self, context):
        """Convert the context dictionary into a JSON object"""
        pickler = Pickler(unpicklable=False)
        return json.dumps(pickler.flatten(context))


class UserProfileAjaxView(JSONResponseMixin, BaseDetailView):
    def get(self, request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseBadRequest()
        context = {
            'username': None, 'profile_icon': None,
            'messages': [{'level': m.tags, 'message': m.message}
                         for m in messages.get_messages(request)],
        }
        if not request.user.is_anonymous():
            profile_icon = 'icon-user'
            if 'auth_provider' in request.session:
                if 'google' == request.session['auth_provider']:
                    profile_icon = 'icon-google-plus-sign'
                elif 'facebook' == request.session['auth_provider']:
                    profile_icon = 'icon-facebook-sign'
                elif 'twitter' == request.session['auth_provider']:
                    profile_icon = 'icon-twitter-sign'
            context.update({
                'username': request.user.username,
                'profile_icon': profile_icon,
            })
        return self.render_to_response(context)


class SmsReportAjaxView(JSONResponseMixin, BaseDetailView):
    def post(self, request, step, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseBadRequest()
        if step == 'sms':
            text = request.POST.get('text', '').strip()[:300]
            if not text:
                return HttpResponseBadRequest()
            request.session['report-text-buffer'] = text
            context = self._step_sms(text)
        elif step == 'organisation':
            org_name = request.POST.get('organisation', '').title()
            if not org_name or 'report-text-buffer' not in request.session:
                return HttpResponseBadRequest()
            context = self._step_org(request, org_name)
        else:
            raise Http404()  # step not found
        return self.render_to_response(context)

    def _step_sms(self, text):
        context = {'slug': None, 'name': None, 'sender': None}
        result = SearchQuerySet().models(SmsSpam).filter(content=text)[:1]
        if not result:
            return context
        result = result[0]
        ratio = SequenceMatcher(None, text, result.text).ratio()
        if ratio > 0.9:
            # found
            context = {
                'slug': result.org_slug,
                'name': result.org_name,
                'sender': result.sender,
            }
        return context

    def _step_org(self, request, org_name):
        org_slug = prepare_slug(org_name, max_char=30)
        org, created = Organisation.objects.get_or_create(
            slug=org_slug,
            defaults={'name': org_name[:50], 'created_by': request.user}
        )
        text = request.session['report-text-buffer']
        del request.session['report-text-buffer']
        spam_slug = prepare_slug(text)
        spam, created = SmsSpam.objects.get_or_create(
            organisation=org, slug=spam_slug,
            defaults={
                'text': text,
                'created_by': request.user,
            }
        )
        SpamReport.objects.get_or_create(spam=spam, user=request.user)
        context = {
            'url': reverse('spam', args=[org_slug, spam_slug])
        }
        messages.success(request, _('Your complaint has been successfully saved.'))
        return context


class InsertTagView(JSONResponseMixin, BaseDetailView):
    def post(self, request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseBadRequest()
        # check POST values
        try:
            sms_id = int(request.POST.get('sms_id'))
            tag_name = request.POST.get('tag', '').lower()
            if not tag_name:
                raise ValueError()
        except ValueError:
            return HttpResponseBadRequest()
        sms = get_object_or_404(SmsSpam, pk=sms_id)
        # create unique tag instance
        tag_slug = str(TagBase().slugify(tag_name))
        tag, tag_created = SpamTag.objects.get_or_create(
            slug=tag_slug, defaults={'name': tag_name}
        )
        # create tag-spam relation user based
        SpamTagRecord.objects.get_or_create(
            content_object=sms, tag=tag, user=request.user,
        )
        tags = sms.tags.values('slug', 'name', 'counter')
        context = {
            'message': _('Your tag has been successfully saved'),
            'tags': tuple(tags),
        }
        return self.render_to_response(context)


class OrganisationLookupView(JSONResponseMixin, BaseDetailView):
    def get(self, request, *args, **kwargs):
        name = request.GET.get('name')
        if not request.is_ajax() or not name:
            return HttpResponseBadRequest()
        organisations = []
        name = unidecode(name).strip().lower()
        org_qset = SearchQuerySet().models(Organisation).autocomplete(content=name)[:10]
        for org in org_qset:
            organisations.append(org.org_name)
        return self.render_to_response(organisations)
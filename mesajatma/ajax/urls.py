#! /usr/bin/env python2.7
from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import ensure_csrf_cookie
from mesajatma.ajax.views import UserProfileAjaxView, SmsReportAjaxView, InsertTagView, OrganisationLookupView


urlpatterns = patterns('',
    url(r'^profile/$', ensure_csrf_cookie(never_cache(UserProfileAjaxView.as_view())),
        name='ajax-profile'),
    url(r'^lookup/org/$', OrganisationLookupView.as_view(), name='ajax-lookup-org'),
    url(r'^add-tag/$', login_required(InsertTagView.as_view()), name='ajax-add-tag'),
    url(r'^report/(?P<step>[a-z]+)/$', login_required(SmsReportAjaxView.as_view()), name='ajax-report'),
)

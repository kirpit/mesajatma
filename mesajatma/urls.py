#! /usr/bin/env python2.7
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from django.contrib.staticfiles.views import serve
from django.contrib import admin
from mesajatma.account.views import AccountProfileView, AccountReportsView, AccountLogoutView
from mesajatma.home.views import HomeView, StaticPageView, StaticFileView
from mesajatma.organisation.views import OrganisationView
from mesajatma.spam.views import SmsSpamView
from mesajatma.stats.views import StatsView

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^(?P<page>.+)\.html$', StaticPageView.as_view(), name='static-view'),
    url(r'^istatistikler/$', StatsView.as_view(), name='stats'),
    url(r'^sirket/(?P<org_slug>.*)/sms/(?P<slug>.*)/$', SmsSpamView.as_view(), name='spam'),
    url(r'^sirket/(?P<slug>.*)/$', OrganisationView.as_view(), name='organisation'),
    url(r'^ajax/', include('mesajatma.ajax.urls')),

    url(r'^accounts/profile/$', login_required(AccountProfileView.as_view()), name='account-profile'),
    url(r'^accounts/reports/$', login_required(AccountReportsView.as_view()), name='account-reports'),
    url(r'^accounts/logout/$', login_required(AccountLogoutView.as_view()), name='account-logout'),
    url(r'^accounts/', include('allauth.urls')),
    # static
    url(r'^(?P<filename>.*)\.(?P<ext>[a-z]{3})$', StaticFileView.as_view()),
    url(r'^%s(?P<path>.*)$' % settings.STATIC_URL.lstrip('/'), serve,
        {'show_indexes': True, 'insecure': False}),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin_tools/', include('admin_tools.urls')),
)

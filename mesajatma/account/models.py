#! /usr/bin/env python2.7
from allauth.account.signals import user_logged_in, user_signed_up
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.db import models


@receiver(user_logged_in, dispatch_uid='auth_provider')
def remember_auth_provider(sender, **kwargs):
    # https://github.com/pennersr/django-allauth/pull/322
    _('Successfully signed in as %(name)s.')
    _('The social account has been connected.')
    _('The social account has been disconnected.')
    request = kwargs['request']
    provider = kwargs['sociallogin'].account.provider
    request.session['auth_provider'] = provider


@receiver(user_signed_up, dispatch_uid='user_profile')
def create_user_profile(sender, **kwargs):
    mobile = ''
    if 'mobile' in kwargs['sociallogin'].account.extra_data:
        mobile = kwargs['sociallogin'].account.extra_data['mobile']
    AccountProfile(user=kwargs['user'], mobile_phone=mobile).save()

mobile_validator = RegexValidator(
    regex=r'^\+\d+$',
    message=_("Only numeric values allowed starting with a plus sign (+)")
)


class AccountProfile(models.Model):
    user = models.OneToOneField(User, primary_key=True, related_name='profile')
    mobile_phone = models.CharField(max_length=20, blank=True, default='',
                                    validators=[mobile_validator])

    def __str__(self):
        return str(self.user)

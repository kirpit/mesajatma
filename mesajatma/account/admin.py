#! /usr/bin/env python2.7
from django.contrib import admin
from mesajatma.account.models import AccountProfile


class AccountProfileAdmin(admin.ModelAdmin):
    raw_id_fields = ('user',)
    pass


admin.site.register(AccountProfile, AccountProfileAdmin)
#! /usr/bin/env python2.7
from django.contrib.auth import logout
from django.core.urlresolvers import reverse
from django.db.models import Count
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView
from mesajatma.account.forms import AccountProfileForm
from mesajatma.account.models import AccountProfile
from mesajatma.organisation.models import Organisation
from mesajatma.report.models import SpamReport
from mesajatma.spam.models import SmsSpam


class AccountLogoutView(TemplateView):
    template_name = 'account/profile.html'

    def get(self, request, *args, **kwargs):
        logout(request)
        return HttpResponseRedirect(reverse('home'))


class AccountProfileView(TemplateView):
    template_name = 'account/profile.html'

    def get(self, request, *args, **kwargs):
        profile, created = AccountProfile.objects.get_or_create(
            user=request.user
        )
        form = AccountProfileForm(initial={
            'email': request.user.email,
            'first_name': request.user.first_name,
            'last_name': request.user.last_name,
            'mobile_phone': profile.mobile_phone,
        })
        context = {
            'form': form,
        }
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        form = AccountProfileForm(request.POST)
        if form.is_valid():
            form.save(request.user)
            form = AccountProfileForm(form.cleaned_data)
        context = {
            'form': form,
        }
        return self.render_to_response(context)


class AccountReportsView(TemplateView):
    template_name = 'account/reports.html'

    def get(self, request, *args, **kwargs):
        org_ids = list()
        sms_ids = list()
        reports = SpamReport.objects.filter(user=request.user).values(
            'organisation',
            'spam',
        )
        for report in reports:
            if report['organisation'] not in org_ids:
                org_ids.append(report['organisation'])
            sms_ids.append(report['spam'])
        # fetch organisation at once
        org_ids.sort()
        sms_ids.sort()
        orgs = Organisation.objects.filter(pk__in=org_ids).values(
            'pk', 'name', 'slug', 'is_customer', 'logo',
        ).annotate(report_count=Count('reports'))
        # fetch sms spams at once
        smss = SmsSpam.objects.filter(id__in=sms_ids).values(
            'organisation', 'slug', 'organisation__slug', 'text',
        )
        context = {
            'orgs': orgs,
            'smss': smss,
        }
        return self.render_to_response(context)

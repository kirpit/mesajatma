#! /usr/bin/env python2.7
import re
from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from crispy_forms.bootstrap import FormActions, StrictButton
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, HTML
from mesajatma.account.models import AccountProfile


class AccountProfileForm(forms.Form):
    email = forms.EmailField(label=_('Email'))
    first_name = forms.CharField(
        max_length=30, required=False, label=_('First name'),
    )
    last_name = forms.CharField(
        max_length=30, required=False, label=_('Last name'),
    )
    mobile_phone = forms.CharField(
        max_length=30, required=False, label=_('Mobile Phone'),
    )
    helper = FormHelper()
    helper.form_class = 'form-horizontal'
    helper.layout = Layout(
        Fieldset(
            _('Personal Information'),
            'email',
            'first_name',
            'last_name',
            'mobile_phone',
            HTML('<p>' +
                 _("We do not share any of your personal info without having your approve.") +
                 '</p>'),
            FormActions(
                StrictButton(
                    _(u"Save"),
                    css_class='btn-large btn-success',
                    type='submit',
                ),
            )
        ),
    )

    def clean_mobile_phone(self):
        value = self.cleaned_data['mobile_phone']
        if value.startswith('05'):
            value = '90' + value[1:]
        elif value.startswith('5'):
            value = '90' + value
        # clean non-numeric chars and add +
        value = re.sub(r'[^\d]', '', value)
        if value:
            value = '+' + value
        return value

    def save(self, user):
        user.email = self.cleaned_data['email']
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.save()
        AccountProfile.objects.filter(user=user).update(
            mobile_phone=self.cleaned_data['mobile_phone'],
        )

    def is_email_registered(self):
        try:
            User.objects.get(email=self.initial['email'])
            return True
        except User.DoesNotExist:
            return False

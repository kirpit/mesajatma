#! /usr/bin/env python2.7
from django.core.urlresolvers import reverse
from django.db.models import Count
from django.utils.translation import ugettext as _
from django.views.generic import TemplateView
from mesajatma.report.models import SpamReport


class StatsView(TemplateView):
    template_name = 'stats.html'

    def get(self, request, *args, **kwargs):
        # prepare all times reports
        all_reports = SpamReport.objects.values(
            'organisation__name', 'organisation__slug',
        ).annotate(
            spam_count=Count('organisation')
        ).order_by('-spam_count')[:10]
        all_times = [{
            'label': _('Company'),
            'value': _('Spam Counts'),
            'link': '',
        }]
        for report in all_reports:
            all_times.append({
                'label': report['organisation__name'],
                'value': report['spam_count'],
                'link': reverse('organisation', args=[report['organisation__slug']]),
            })
            pass
        # add stats to context
        context = {
            'all_times': all_times,
        }
        return self.render_to_response(context)

#! /usr/bin/env python2.7
from haystack import indexes
from mesajatma.spam.models import SmsSpam


class SmsSpamIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(model_attr='text', document=True)
    sender = indexes.CharField(model_attr='sender')
    # TODO: add tags to spam search index:
    # tags = indexes.MultiValueField()
    spam_slug = indexes.CharField(model_attr='slug', indexed=False)
    is_active = indexes.BooleanField(model_attr='is_active', indexed=False)
    org_name = indexes.CharField(model_attr='organisation__name')
    org_slug = indexes.CharField(model_attr='organisation__slug', indexed=False)

    def get_model(self):
        return SmsSpam

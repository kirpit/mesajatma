#! /usr/bin/env python2.7
from django.core.urlresolvers import reverse
from django.db.models import Count
from django.http import HttpResponsePermanentRedirect
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView
from mesajatma.organisation.models import Organisation
from mesajatma.spam.models import SmsSpam


class SmsSpamView(TemplateView):
    template_name = 'smsspam.html'

    def get(self, request, org_slug, slug, *args, **kwargs):
        # fetch org
        try:
            org = Organisation.objects.annotate(
                report_count=Count('reports'),
            ).get(slug=org_slug)
        except Organisation.DoesNotExist:
            org = get_object_or_404(Organisation, old_slug=org_slug)
            return HttpResponsePermanentRedirect(reverse(
                'spam', args=[org.slug, slug],
            ))
        # fetch sms
        try:
            sms = SmsSpam.objects.select_related().annotate(
                report_count=Count('reports'),
            ).get(organisation=org, slug=slug, is_active=True)
        except SmsSpam.DoesNotExist:
            sms = get_object_or_404(
                SmsSpam, organisation=org, old_slug=slug, is_active=True,
            )
            return HttpResponsePermanentRedirect(reverse(
                'spam', args=[org.slug, sms.slug],
            ))
        context = {
            'org': org,
            'sms': sms,
            'sms_tags': sms.tags.filter(is_active=True),
        }
        return self.render_to_response(context)
#! /usr/bin/env python2.7
from django.contrib import admin
from taggit.models import Tag
from mesajatma.spam.models import SmsSpam, SpamTagRecord, SpamTag


class SmsMessageAdmin(admin.ModelAdmin):
    raw_id_fields = ('organisation', 'created_by',)
    exclude = ('type', 'tags', )
    search_fields = ('organisation__name', 'text', )
    list_filter = ('is_active', 'type', )


# class EmailMessageAdmin(admin.ModelAdmin):
#     raw_id_fields = ('organisation', 'created_by',)
#     exclude = ('type', )


admin.site.unregister(Tag)
admin.site.register(SpamTag)
admin.site.register(SpamTagRecord)
admin.site.register(SmsSpam, SmsMessageAdmin)
# admin.site.register(EmailSpam, EmailMessageAdmin)
#! /usr/bin/env python2.7
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models
from taggit.managers import TaggableManager
from taggit.models import ItemBase, TagBase
from mesajatma.organisation.models import Organisation


class SpamTag(TagBase):
    counter = models.PositiveIntegerField(default=0)
    is_active = models.BooleanField(default=True, db_index=True)


class SpamTagRecord(ItemBase):
    content_object = models.ForeignKey('Spam')
    tag = models.ForeignKey(SpamTag,
                            related_name="%(app_label)s_%(class)s_items")
    user = models.ForeignKey(User, null=True)

    class Meta:
        unique_together = (
            ('content_object', 'tag', 'user'),
        )

    def save(self, using=None, *args, **kwargs):
        if not self.pk:
            self.tag.counter += 1
            self.tag.save(using=using)
        return super(SpamTagRecord, self).save(*args, **kwargs)

    @classmethod
    def tags_for(cls, model, instance=None):
        if instance is not None:
            return cls.tag_model().objects.filter(**{
                '%s__content_object' % cls.tag_relname(): instance
            })
        return cls.tag_model().objects.filter(**{
            '%s__content_object__isnull' % cls.tag_relname(): False
        }).distinct()


class Spam(models.Model):
    organisation = models.ForeignKey(Organisation, related_name='spams')
    tags = TaggableManager(through=SpamTagRecord, blank=True)
    type = models.CharField(max_length=20)
    old_slug = models.SlugField(blank=True)
    slug = models.SlugField()
    is_active = models.BooleanField(default=True, db_index=True)
    created_by = models.ForeignKey(User, blank=True, null=True,
                                   on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-created_at', )
        index_together = (
            ('is_active', 'created_at'),
            ('organisation', 'slug'),
            ('organisation', 'old_slug'),
        )
        unique_together = (
            ('organisation', 'slug'),
        )

    def __str__(self):
        return self.slug

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.type = self.__class__.__name__.lower()
        return super(Spam, self).save(
            force_insert, force_update, using, update_fields,
        )

    def get(self):
        return getattr(self, self.type)

    @property
    def org(self):
        return self.organisation

    def get_absolute_url(self):
        return reverse('spam', args=[self.org.slug, self.slug])


class SmsSpam(Spam):
    sender = models.CharField(max_length=20, blank=True, default='')
    text = models.TextField(max_length=300)


# class EmailSpam(Spam):
#     screen_shot = models.ImageField(upload_to='spams/emails/')


# class VoiceMailSpam(Spam):
#     record = models.FileField(upload_to='spams/voicemails/')

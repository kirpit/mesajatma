#! /usr/bin/env python2.7
from django.conf import settings
from django.http import Http404
from django.views.generic import TemplateView, RedirectView
from django.template import TemplateDoesNotExist
from mesajatma.report.models import SpamReport
from mesajatma.spam.models import SmsSpam


class HomeView(TemplateView):
    template_name = 'home.html'

    def get(self, request, *args, **kwargs):
        last_reports = SpamReport.objects.filter(spam__is_active=True).values(
            'spam__slug', 'spam__smsspam__text', 'organisation__slug', 'organisation__name',
            'organisation__logo',
        )[:3]
        context = {
            'last_reports': last_reports,
        }
        return self.render_to_response(context)


class StaticPageView(TemplateView):
    def get(self, request, page, *args, **kwargs):
        self.template_name = 'static/%s.html' % page
        response = super(StaticPageView, self).get(request, *args, **kwargs)
        try:
            return response.render()
        except TemplateDoesNotExist:
            raise Http404()


class StaticFileView(RedirectView):
    def get(self, request, filename, ext, *args, **kwargs):
        if ext not in ('ico', 'png', 'txt'):
            raise Http404()
        subfolder = ''
        if ext in ('ico', 'png'):
            subfolder = 'img/ico/'
        self.url = '{static_url}{subfolder}{filename}.{ext}'.format(
            static_url=settings.STATIC_URL,
            subfolder=subfolder,
            filename=filename,
            ext=ext,
        )
        return super(StaticFileView, self).get(request)
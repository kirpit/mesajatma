#! /usr/bin/env python2.7
from django.utils.encoding import smart_str
from django.utils.text import Truncator, slugify
from unidecode import unidecode


def make_cache_key(key, key_prefix, version):
    """
    Memcached page caching key generator
    """
    return smart_str(key)


def prepare_slug(text, max_char=50):
    prepared = text.replace(',', ' ').replace('.', ' ').replace('!', ' ')\
        .replace('+', ' ')
    prepared = unidecode(prepared)
    return slugify(Truncator(prepared).words(6, truncate=''))[:max_char]


#! /usr/bin/env python2.7
from django.template import Library
from django.template.defaultfilters import safe
from jsonpickle import encode

register = Library()

@register.filter
def jsonify(obj):
    return safe(encode(obj, unpicklable=False))
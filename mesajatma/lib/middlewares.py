#! /usr/bin/env python2.7
import re
from django.conf import settings
from django.core.cache import cache


class NginxMemcachedMiddleware(object):
    """
    Creates memcached entry on demand from Django middleware
    for Nginx to serve as frontend

    e.g. should be preferably applied to the middleware classes
    -at the beginning- so can be processed at last (say after GZipMiddleware)
    
    http://tabbedthinking.posterous.com/nginx-memcached-uwsgi-django
    http://soyrex.com/articles/django-nginx-memcached.html
    """
    def process_response(self, request, response):
        # if it's NOT a GET then do not cache it
        if response.status_code != 200 or request.method != 'GET':
            return response
        # loop on our settings.CACHE_IGNORE_RE and ignore certain urls
        request_path = request.get_full_path()
        for exp in settings.CACHE_IGNORE_RE:
            if re.match(exp, request_path):
                return response
        key = request.get_host() + request_path
        cache.set(key, response.content)
        return response

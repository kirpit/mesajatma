# CoffeeScript file that should be compiled into main.js

$loginLink = $('ul#bar-menu > li').last()
$stepGuides = $('#steps > li')
$stepPanes = $('.step-pane')
$addTagForm = $('form#add-tag-form')
csrf_token = $.cookie('csrftoken').read()


jumpToStep = (step) ->
  $stepPanes.removeClass('active')
  $stepPanes.eq(step).addClass('active')
  $stepGuides.each (s, guide) ->
    if s < step
      $(guide).removeClass('active').addClass('complete')
      $(guide).children('span.badge').removeClass('badge-warning').addClass('badge-success')
  $stepGuides.eq(step).addClass('active')
  $stepGuides.eq(step).children('span.badge').addClass('badge-warning')


# TAG CANVAS
redrawTagCanvas = () ->
  $('.tags-canvas').tagcanvas
      textColour: 'darkorange'
      shape: 'hcylinder'
      lock: 'x'
      initial: [0, -0.05]
      wheelZoom: false
      noSelect: true
      depth : 0.01
      maxSpeed: 0.1
      weight: true

# FLASH MESSAGES
window.flashMessage = (msg) ->
  $alert = $('.alert').clone()
  $alert.addClass("alert-#{ msg.level }")
  $alert.append("<p>#{ msg.message }</p>")
  $('#alert-container').append($alert)
  $alert.fadeIn('slow')
  window.setTimeout(
    () ->
      $alert.fadeOut 'slow', () ->
        $(this).remove()
    5000
  )


window.drawColumnChart = (container, arrayData, title=null) ->
  labels = (d.label for d in arrayData)
  values = (d.value for d in arrayData)
  links = (d.link for d in arrayData if d.link?) or []
  links = null unless links.length is labels.length
  data = google.visualization.arrayToDataTable [
    labels,
    values,
  ]
  chart = new google.visualization.ColumnChart($(container).get(0))

  selectHandler = () ->
    selections = chart.getSelection()
    if links? and not (selections.length is 1 and 'row' not in Object.keys(selections[0]))
      return
    link = links[selections[0]['column']]
    window.document.location = link

  google.visualization.events.addListener(chart, 'select', selectHandler);

  chart.draw data,
    title : title

# document onReady BEGIN
$ ->
  redrawTagCanvas()

  # TEMP Coming soon
  $('#stats').tooltip
    placement: 'bottom'

  # USER SPECIFIC AJAX REQ
  ajax = $.ajax
    url: AJAX_URL.PROFILE
  ajax.success (user) ->
    if user.messages
      for m in user.messages
        window.flashMessage(m)
    if not user.username
      $loginLink.show()
      jumpToStep(0)
    else
      # my account menu
      $loginLink.remove()
      $myAccount = $('li#bar-menu-myaccount')
      $myAccount.find('.dropdown-toggle > i').addClass(user.profile_icon)
      $('ul#bar-menu').append($myAccount.show())
      # jump to step 2
      jumpToStep(1)
      $('textarea[name="report-text"]').focus()
      # activate add-tag
      $addTagForm.show()

  # ADD-TAG SUBMIT
  $addTagForm.submit (e) ->
    e.preventDefault();
    $tagInput = $(this).find('input[name="tag"]')
    ajax = $.ajax
      type: $(this).attr('method')
      url: $(this).attr('action')
      data:
        csrfmiddlewaretoken: csrf_token
        sms_id: $(this).find('input[name="sms_id"]').val()
        tag: $tagInput.val()
    ajax.success (data) ->
      $tagInput.val('')
      $tagList = $('canvas.tags-canvas > ul')
      $tagList.children().remove()
      for tag in data.tags
        newTag = "<li><a href='#' data-weight='#{tag.counter}'>#{tag.name}</a></li>"
        $tagList.append(newTag)
      redrawTagCanvas()
      console.log(data)

  # SEND SMS TEXT SUBMIT
  $('form#report-form-text').submit (e) ->
    e.preventDefault()
    text = $(this).find('textarea[name="report-text"]').val()
    if not text
      return
    ajax = $.ajax
      type: $(this).attr('method')
      url: $(this).attr('action')
      data:
        csrfmiddlewaretoken: csrf_token
        text: text
    ajax.success (org) ->
      $('#report-form-org').find('input[name="report-org"]').val(org.name)
      # hide stats summary
      $('#stats-summary').addClass('visible-desktop')
      # jump to step 3
      jumpToStep(2)

  # SEND ORGANISATION NAME SUBMIT
  $('#report-form-org').submit (e) ->
    e.preventDefault()
    organisation = $('#report-form-org').find('input[name="report-org"]').val()
    if not organisation
      return
    ajax = $.ajax
      type: $(this).attr('method')
      url: $(this).attr('action')
      data:
        csrfmiddlewaretoken: csrf_token
        organisation: organisation
    ajax.success (data) ->
      window.document.location.href = data.url

  # ORGANISATION LOOKUP
  org_name_cache = null
  $lookups = $('input.org-lookup')
  $lookups.attr('autocomplete', 'off')
  # lookup function
  orgLookup = (name, process) ->
    if org_name_cache is name
      return
    org_name_cache = name
    ajax = $.ajax
      type: 'get'
      url: AJAX_URL.ORG_LOOKUP
      data:
        name: name
    ajax.success (orgs) ->
      process(orgs)
  # set typeahead
  $lookups.typeahead
    source: orgLookup
    items: 10
    minLength: 2

  # UPLOAD LOGO GADGET
  $('a.add-logo').click (e) ->
    e.preventDefault()
    $('#add-logo-modal').find('h3').text()
    $('#add-logo-modal').modal()
    console.log('add logo')
